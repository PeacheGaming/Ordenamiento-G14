// Ordenamientos.cpp: define el punto de entrada de la aplicaci�n de consola.
//

#include <iostream>
#include <windows.h>
using namespace std;
// ESTRUCTURAS
struct nodo_s {
	int valor;
	nodo_s *sgt;
};
struct nodo {
	int valor;
	nodo *ant;
	nodo *sgte;
};
typedef nodo_s *cola;
typedef nodo *lista_doble;

//
int Total=0;
void limpiarBuffer();

//COMPLEMENTO LISTAS
lista_doble crear_nodo(int valor) {
	lista_doble aux = new(nodo);
	aux->ant = NULL;
	aux->sgte = NULL;
	aux->valor = valor;
	return aux;
}
//

//COMPLEMENTO COLA
cola crear_nodo_c(int valor) {
	cola aux = new(nodo_s);
	aux->sgt = NULL;
	aux->valor = valor;
	return aux;
}
//

// FUNCIONES COLAS
void adicionar(cola &c,int valor) {
	cola aux = crear_nodo_c(valor);
	if (c == NULL) {
		c = aux;
	}
	else {
		cola temp = c;
		while (temp->sgt != NULL)
			temp = temp->sgt;
		temp->sgt = aux;
	}
}
int extraer(cola &c) {
	int aux=c->valor;
	c = c->sgt;
	return aux;
}
//

//FUNCIONES EXTRAS
int digito(int valor, int i) {
	int j = 0,dig=0;
	while (j < i) {
		dig = valor % 10;
		valor = valor / 10;
		j++;
	}
	return dig;
}
void destruir(lista_doble &l1){
	lista_doble aux=NULL;
	while(l1!=NULL){
		aux=l1;
		l1=l1->sgte;
		delete(aux);
	}
}

int len(lista_doble l){
	int n=0;
	while(l!=NULL){
		n++;
		l=l->sgte;
	}
	return n;
}
//

//FUNCIONES LISTAS 
void ingresar_inicio(lista_doble &l1, int valor) {
	lista_doble aux = crear_nodo(valor);
	if (l1 == NULL)
		l1 = aux;
	else {
		aux->sgte = l1;
		l1->ant = aux;
		l1 = aux;
	}
	Total++;
}
void ingresar_final(lista_doble &l1, int valor) {
	lista_doble aux = crear_nodo(valor), temp = NULL;
	if (l1 == NULL)
		l1 = aux;
	else {
		temp = l1;
		while (temp->sgte != NULL) {
			temp = temp->sgte;
		}
		temp->sgte = aux;
		aux->ant = temp;
	}
	Total++;
}
lista_doble rellenar() {
	lista_doble l1 = NULL;
	ingresar_inicio(l1, 12);
	ingresar_inicio(l1, 22);
	ingresar_inicio(l1, 72);
	ingresar_final(l1, 25);
	ingresar_final(l1, 67);
	return l1;
}
void ingresar_Posicion(lista_doble &l1, int valor, int pos) {
	lista_doble aux = crear_nodo(valor), temp = NULL;
	int i = 0;
	if (pos == 1) {
		ingresar_inicio(l1, valor);
	}
	else {
		temp = l1;
		while (temp->sgte != NULL && i<pos - 1) {
			temp = temp->sgte;
			i++;
		}
		aux->sgte = temp;
		aux->ant = temp->ant;
		temp->ant->sgte = aux;
		temp->ant = aux;
	}
	Total++;
}
void eliminar_inicio(lista_doble &l1) {
	lista_doble temp = NULL;
	if (l1 == NULL) {
		cout << "\t\t\tNO EXISTE LISTA";
		system("pause>nul");
	}
	else {
		temp = l1;
		l1 = l1->sgte;
		if (l1 != NULL)
			l1->ant = NULL;
		delete(temp);
		Total--;
	}
	
}
void eliminar_final(lista_doble &l1) {
	lista_doble temp = NULL;
	if (l1 == NULL) {
		cout << "\t\t\tNO EXISTE LISTA";
		system("pause>nul");
	}
	else {
		temp = l1;
		while (temp->sgte != NULL) {
			temp = temp->sgte;
		}
		if (temp->ant != NULL) {
			temp->ant->sgte = NULL;
			temp->ant = NULL;
		}
		else {
			l1 = NULL;
		}
		delete(temp);
		Total--;
	}
}
void eliminar_posicion(lista_doble &l1, int pos) {
	lista_doble temp = NULL;
	int i = 0;
	if (pos == 1 || l1 == NULL){
		eliminar_inicio(l1);
		Total--;
		}
	else {
		if(pos==Total)
			eliminar_final(l1);
		else{
			temp = l1;
			while (temp->sgte != NULL && i<pos - 1) {
				temp = temp->sgte;
				i++;
			}
			temp->ant->sgte = temp->sgte;
			temp->sgte->ant = temp->ant;
			delete(temp);
			Total--;
		}
	}
}
lista_doble intercalar(lista_doble ordizq,lista_doble ordder){
	lista_doble result=NULL;
	while(ordizq!=NULL && ordder!=NULL){
		if(ordizq->valor<ordder->valor){
			ingresar_final(result,ordizq->valor);
			ordizq=ordizq->sgte;
		}
		else{
			ingresar_final(result,ordder->valor);
			ordder=ordder->sgte;
		}
	}
	while(ordizq!=NULL){
		ingresar_final(result,ordizq->valor);
		ordizq=ordizq->sgte;
	}
	while(ordder!=NULL){
		ingresar_final(result,ordder->valor);
		ordder=ordder->sgte;
	}
	destruir(ordizq);
	destruir(ordder);
	return result;
}
//

//ORDENAMIENTOS LISTAS
void Burbuja(lista_doble &l1) {
	lista_doble temp = l1;
	int aux;
	bool estado = false;
	while (estado || temp->sgte != NULL) {
		if (temp->sgte == NULL) {
			temp = l1;
			estado = false;
		}
		if (temp->valor>temp->sgte->valor) {
			aux = temp->valor;
			temp->valor = temp->sgte->valor;
			temp->sgte->valor = aux;
			estado = true;
		}
		temp = temp->sgte;
	}
}
void Insercion_normal(lista_doble &l1) {
	lista_doble temp = l1->sgte , temp1=NULL;
	int aux;
	do {
		temp1 = temp;
		while (temp1->ant!= NULL && temp1->ant->valor > temp1->valor) {
			aux = temp1->valor;
			temp1->valor = temp1->ant->valor;
			temp1->ant->valor = aux;

			temp1 = temp1->ant;
		}
		temp = temp->sgte;
	} while (temp!= NULL);
}
void Seleccion(lista_doble &l1) {
	lista_doble inicio = l1, temp = NULL, aux = NULL;
	int auxi;
	while (inicio->sgte != NULL) {
		temp = inicio;
		aux = temp;
		while (temp->sgte != NULL) {
			if (aux->valor>temp->sgte->valor)
				aux = temp->sgte;
			temp = temp->sgte;
		}

		auxi = aux->valor;
		aux->valor = inicio->valor;
		inicio->valor = auxi;

		inicio = inicio->sgte;
	}
}
void Insercion(lista_doble &l1, int valor) {
	lista_doble aux = crear_nodo(valor), temp = NULL;
	bool estado = true;
	if (l1 == NULL) {
		l1 = aux;
	}
	else {
		if (valor<l1->valor)
			ingresar_inicio(l1, valor);
		else {
			temp = l1;
			while (temp->sgte != NULL && estado) {
				temp = temp->sgte;
				if (temp->ant->valor<aux->valor && aux->valor<temp->valor)
					estado = false;
			}
			if (temp->sgte == NULL) {
				temp->sgte = aux;
				aux->ant = temp;
			}
			else {
				temp->ant->sgte = aux;
				aux->ant = temp->ant;
				temp->ant = aux;
				aux->sgte = temp;
			}
		}
	}
}
void Intercambio(lista_doble &l1) {
	lista_doble inicio = l1, temp = NULL, aux = NULL;
	int auxi;
	while (inicio->sgte != NULL) {
		temp = inicio;
		aux = temp;
		while (temp->sgte != NULL) {
			if (aux->valor > temp->sgte->valor) {
				auxi = aux->valor;
				aux->valor = temp->sgte->valor;
				temp->sgte->valor = auxi;
			}
			temp = temp->sgte;
		}
		inicio = inicio->sgte;
	}
}
lista_doble seleccion(lista_doble l1,int i){
     for(int j=0;j<i;j++)
        l1=l1->sgte;
     return l1;
}
void shell(lista_doble&l1){
   int salto, aux, i;
   boolean cambios;
   for(salto=Total; salto!=0; salto/=2){
       cambios=true;
       while(cambios){
           cambios=false;
           for(i=salto; i<Total; i++)
                   if(seleccion(l1,i-salto)->valor>seleccion(l1,i)->valor){
                     aux=seleccion(l1,i)->valor;
                     seleccion(l1,i)->valor=seleccion(l1,i-salto)->valor;
                     seleccion(l1,i-salto)->valor=aux;
                     cambios=true;
                   }
            }
        }
}
int particion(lista_doble &l1, int izq, int der){
      int i = izq, j = der;
      int tmp;
      int pivot = seleccion(l1,(izq+der)/2)->valor;
      while (i <= j) {
            while (seleccion(l1,i)->valor < pivot)
                  i++;
            while (seleccion(l1,j)->valor > pivot)
                  j--;
            if (i <= j) {
                  tmp = seleccion(l1,i)->valor;
                  seleccion(l1,i)->valor= seleccion(l1,j)->valor;
                  seleccion(l1,j)->valor = tmp;
                  i++;
                  j--;
            }
      }
      return i;
}
void quickSort(lista_doble &l1, int izq, int der) {
      int indice = particion(l1, izq,der);
      if (izq< indice - 1)
            quickSort(l1, izq, indice - 1);
      if (indice < der)
            quickSort(l1, indice, der);
}
void QuickSort(lista_doble &l1){
   quickSort(l1,0,Total-1);
}
void radix(lista_doble &l1) {
	lista_doble temp = l1,final;
	cola c[10] = { NULL };
	int valor,i=1;
	boolean estado;
	do{
		estado = false;
		final=NULL;
		while (temp != NULL) {
			valor = digito(temp->valor, i);
			adicionar(c[valor], temp->valor);
			if(valor!=0)
			estado = true;
			temp = temp->sgte;
		}
		for (int j = 0; j < 10; j++) {
			while (c[j] != NULL) {
				ingresar_final(final, extraer(c[j]));
			}
		}
		temp=final;
		i++;
	}while (estado) ;
	l1=temp;
}
lista_doble mergeSort(lista_doble &l1){
	int tamano=len(l1);
	if(tamano<=1){
		return l1;
	}
	else{
		lista_doble izq=l1,der=l1;
		int medio= tamano/2;
		for(int i=1;i<=medio;i++){
			der=der->sgte;
		}
		der->ant->sgte=NULL;
		der->ant=NULL;
		izq=mergeSort(izq);
		der=mergeSort(der);
		return intercalar(izq,der);
	}
}
void MergeSort(lista_doble &l1){
	l1=mergeSort(l1);
}
//

// MOSTRAR
void mostrar(lista_doble l1) {
	cout << "\t\t\t->";
	while (l1 != NULL) {
		cout << l1->valor << "->";
		l1 = l1->sgte;
	}
	cout << "NULL";
}
//

//OPCIONES DEL MENU
void opcion1(lista_doble &l1) {
	int valor;
	cout << "\t\t\tIngrese el valor: "; cin >> valor;
	if (!cin.fail()) {
		ingresar_inicio(l1, valor);
	}
	else {
		cout << "\t\t\tVALOR INVALIDO!" << endl;
		limpiarBuffer();
	}
}
void opcion2(lista_doble &l1) {
	int valor;
	cout << "\t\t\tIngrese el valor: "; cin >> valor;
	if (!cin.fail()) {
		ingresar_final(l1, valor);
	}
	else {
		cout << "\t\t\tVALOR INVALIDO!" << endl;
		limpiarBuffer();
	}
}
void opcion3(lista_doble &l1) {
	int valor, posicion;
	cout << "\t\t\tIngrese el valor: "; cin >> valor;
	if (!cin.fail()) {
		cout << "\t\t\tIngrese posicion: "; cin >> posicion;
		if (!cin.fail() && 0<posicion && posicion<=Total)
			ingresar_Posicion(l1, valor, posicion);
		else {
			cout << "\t\t\tPOSICION INVALIDA!" << endl;
			limpiarBuffer();
		}
	}
	else {
		cout << "\t\t\tVALOR INVALIDO!" << endl;
		limpiarBuffer();
	}
}
void opcion4(lista_doble &l1) {
	if (l1 != NULL) {
		eliminar_inicio(l1);
		cout << "\t\t\tEliminacion EXITOSA!" << endl;
	}else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion5(lista_doble &l1) {
	if (l1 != NULL) {
		eliminar_final(l1);
		cout << "\t\t\tEliminacion EXITOSA!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion6(lista_doble &l1) {
	int posicion;
	cout << "\t\t\tIngrese posicion: "; cin >> posicion;
	if(l1!=NULL)
		if (!cin.fail() && 0<posicion && posicion<=Total) {
			eliminar_posicion(l1, posicion);
			cout << "\t\t\tELIMINACION EXITOSA" << endl;
			system("pause>nul");
		}
		else {
			cout << "\t\t\tPOSICION INVALIDA!" << endl;
			limpiarBuffer();
		}
	else{
		cout << "\t\t\tLISTA VACIA!" << endl;
     	system("pause>nul");
    }
}
void opcion7(lista_doble &l1) {
	if (l1 != NULL) {
		Burbuja(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else 
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion8(lista_doble &l1) {
	if (l1 != NULL) {
		Seleccion(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion9(lista_doble &l1) {
	if (l1 != NULL) {
		Insercion_normal(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion10(lista_doble &l1) {
	int valor;
	cout << "\t\t\tIngrese el valor por insercion: "; cin >> valor;
	if (!cin.fail()) {
		Insercion(l1, valor);
	}
	else {
		cout << "\t\t\tVALOR INVALIDO!" << endl;
		limpiarBuffer();
	}
}
void opcion11(lista_doble &l1) {
	if (l1 != NULL) {
		Intercambio(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion12(lista_doble &l1) {
	if (l1 != NULL) {
		radix(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion13(lista_doble &l1) {
	if (l1 != NULL) {
		shell(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion14(lista_doble &l1) {
	if (l1 != NULL) {
		QuickSort(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion15(lista_doble &l1){
	if (l1 != NULL) {
		MergeSort(l1);
		cout << "\t\t\tORDENAMIENTO EXITOSO!" << endl;
	}
	else
		cout << "\t\t\tLISTA VACIA!" << endl;
	system("pause>nul");
}
void opcion_mostrar(lista_doble l1) {
	mostrar(l1);
	system("pause>nul");
}
//

//  MENU
int menu(lista_doble &l1) {
	system("cls");
	int p;
	cout << "\t\t\t������������������������������������ͻ" << endl;
	cout << "\t\t\t�                MENU                �" << endl;
	cout << "\t\t\t������������������������������������͹" << endl;
	cout << "\t\t\t�  1. Ingresar al inicio             �" << endl;
	cout << "\t\t\t�  2. Ingresar al final              �" << endl;
	cout << "\t\t\t�  3. Ingresar por posicion          �" << endl;
	cout << "\t\t\t�  4. Eliminar al inicio             �" << endl;
	cout << "\t\t\t�  5. Eliminar al final              �" << endl;
	cout << "\t\t\t�  6. Eliminar por posicion          �" << endl;
	cout << "\t\t\t�  -----------ORDENAMIENTOS----------�" << endl;
	cout << "\t\t\t�  7. Ordenar por Burbuja            �" << endl;
	cout << "\t\t\t�  8. Ordenar por Seleccion          �" << endl;
	cout << "\t\t\t�  9. Ordenar por Insercion          �" << endl;
	cout << "\t\t\t�  10. Ordenar por Insercion por     �" << endl;
	cout << "\t\t\t�     Valor Ingresado                �" << endl;
	cout << "\t\t\t�  11. Ordenar por Intercambio       �" << endl;
	cout << "\t\t\t�  12. Ordenar por Radix             �" << endl;
    cout << "\t\t\t�  13. Ordenar por Shell             �" << endl;
    cout << "\t\t\t�  14. Ordenar por QuickSort         �" << endl;
    cout << "\t\t\t�  15. Ordenar por mergeSort         �" << endl;
	cout << "\t\t\t�  16. Mostrar elementos             �" << endl;
	cout << "\t\t\t�  17. Salir                         �" << endl;
	cout << "\t\t\t������������������������������������ͼ" << endl;
	cout << "\t\t\t\tIngrese una opcion: ";	cin >> p;
	switch (p) {
	case 1: opcion1(l1); break;
	case 2: opcion2(l1); break;
	case 3: opcion3(l1); break;
	case 4: opcion4(l1); break;
	case 5: opcion5(l1); break;
	case 6: opcion6(l1); 	break;
	case 7: opcion7(l1); break;
	case 8: opcion8(l1); break;
	case 9: opcion9(l1); break;
	case 10: opcion10(l1); break;
	case 11: opcion11(l1); break;
	case 12: opcion12(l1); break;
	case 13: opcion13(l1); break;
	case 14: opcion14(l1); break; 
	case 15: opcion15(l1); break;
	case 16:opcion_mostrar(l1); break;
	case 17:	 exit(0); break;
	default: cout << "Opcion invalida!\n";
		limpiarBuffer();
		break;
	}
	menu(l1);
}
//

//COMPLEMENTO: LIMPIARBUFFER
void limpiarBuffer() {
	system("pause>nul");
	std::cin.clear();
	std::cin.ignore(1000, '\n');
}
//

//PRINCIPAL
int main() {
	lista_doble l1 = rellenar();
	menu(l1);
}
//
